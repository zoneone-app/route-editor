import {Box, Grid} from "@chakra-ui/react";
import React, {FC} from "react";

export const Layout: FC = props => (
    <Grid
        templateColumns={["1fr", "1fr 30rem 1fr", "1fr 48rem 1fr"]}
        gridTemplateRows="4em auto"
    >
        <Box
            gridColumn={2} gridRow={2}
            borderRadius={16}
            boxShadow="base"
            backgroundColor="white"
        >
            {props.children}
        </Box>
    </Grid>
);

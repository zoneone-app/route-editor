import React, {FC} from "react";
import logo from "../assets/svg/logo.svg";
import {Flex, Heading, Image, ThemingProps, useMultiStyleConfig} from "@chakra-ui/react";

export const style = {
    parts: ["icon", "label"],

    baseStyle: {
        icon: {
            m: 2
        }
    },

    sizes: {
        sm: {
            icon: {
                boxSize: 4
            },
            label: {
                textStyle: "title-sm"
            }
        },
        lg: {
            icon: {
                boxSize: 8
            },
            label: {
                textStyle: "title-md"
            }
        }
    },

    defaultProps: {
        size: "sm"
    }
};

export interface LogoProps {
    withLabel?: boolean;
    name?: string;
}

export const Logo: FC<ThemingProps & LogoProps> = props => {
    const {size, variant, colorScheme, withLabel} = props;
    const styles = useMultiStyleConfig("Logo", {size, variant, colorScheme});

    // TODO: There must be a better way to do this
    // @ts-ignore
    const textStyle = styles.label.textStyle as string;

    return (withLabel ? (
        <Flex align="center">
            <Image src={logo} sx={styles.icon}/>
            <Heading sx={styles.label} textStyle={textStyle}>ZoneONE {props.name && `- ${props.name}`}</Heading>
        </Flex>
    ) : (
        <Image src={logo} sx={styles.icon}/>
    ));
};

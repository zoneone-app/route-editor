const withImage = require("next-images");

module.exports = withImage({
    target: "serverless"
});

import React, {FC} from "react";
import {ChakraProvider} from "@chakra-ui/react";
import {AppProps} from "next/app";
import {theme} from "../style/theme";
import Head from "next/head";

const App: FC<AppProps> = ({Component, pageProps}) => (
    <>
        <Head>
            <title>Route Editor | ZoneONE</title>
        </Head>
        <ChakraProvider theme={theme}>
            <Component />
        </ChakraProvider>
    </>
);

export default App;

import {Center} from "@chakra-ui/react";
import React, {FC} from "react";
import {Layout} from "../components/Layout";
import {Logo} from "../components/Logo";

const Index: FC = props => (
    <Layout>
        <Center height={32}>
            <Logo size="lg" withLabel name="Route Editor"/>
        </Center>
    </Layout>
);

export default Index;

import {extendTheme} from "@chakra-ui/react";
import "@fontsource/fira-sans";
import "@fontsource/roboto";
import {style as Logo} from "../components/Logo";

export const theme = extendTheme({
    components: {
        Logo
    },

    styles: {
        global: {
            body: {
                background: "gray.50"
            }
        }
    },

    colors: {
        // https://material.io/inline-tools/color/
        brand: {
            50: "#ffebed",
            100: "#ffcdd0",
            200: "#f09b96",
            300: "#e6756d",
            400: "#ef5648",
            500: "#f3492b",
            600: "#e43f2b",
            700: "#d33525",
            800: "#c62e1e",
            900: "#b72311"
        }
    },

    fonts: {
        heading: "Fira Sans",
        body: "Roboto"
    },

    textStyles: {
        "routes/number-lg": {
            fontWeight: "800",
            fontSize: "2rem",
            lineHeight: "121%"
        },
        "routes/prefix-lg": {
            fontWeight: "600",
            fontSize: "1.625rem",
            lineHeight: "121%"
        },
        "routes/number-md": {
            fontWeight: "800",
            fontSize: "1rem",
            lineHeight: "121%"
        },
        "routes/prefix-md": {
            fontWeight: "600",
            fontSize: "0.875rem",
            lineHeight: "121%"
        },
        "routes/number-sm": {
            fontWeight: "800",
            fontSize: "0.75rem",
            lineHeight: "121%"
        },
        "routes/prefix-sm": {
            fontWeight: "600",
            fontSize: "0.625rem",
            lineHeight: "121%"
        },

        "title-xl": {
            fontSize: "2.125rem",
            lineHeight: "121%"
        },
        "title-lg": {
            fontSize: "1.75rem",
            lineHeight: "121%"
        },
        "title-md": {
            fontSize: "1.375rem",
            lineHeight: "121%"
        },
        "title-sm": {
            fontSize: "1.25rem",
            lineHeight: "121%"
        },
        headline: {
            fontSize: "1.0625rem",
            lineHeight: "121%",
            fontWeight: "bold"
        },
        body: {
            fontSize: "1.0625rem",
            lineHeight: "121%"
        },
        callout: {
            fontSize: "1rem",
            lineHeight: "121%"
        },
        subheadline: {
            fontSize: "0.9375rem",
            lineHeight: "121%"
        },
        footnote: {
            fontSize: "0.8125rem",
            lineHeight: "121%"
        },
        caption: {
            fontSize: "0.75rem",
            lineHeight: "121%"
        },
        "caption-sm": {
            fontSize: "0.6875rem",
            lineHeight: "121%"
        }
    },

    config: {
        useSystemColorMode: true
    }
});
